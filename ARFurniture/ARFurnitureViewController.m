//
//  ARFurnitureViewController.m
//  ARFurniture
//
//  Created by dilitech on 14-7-16.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "ARFurnitureViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ARRootViewController.h"

//#import <DilitechRequest.framework/Headers/DR.h>
#import "MJRefresh/MJRefresh.h"
#import "UIButton+openTapArea.h"

#import "ARFurnitureCell.h"
#import "ARFunitureListCell.h"

#import "ARListCellVC.h"



@class CustomTableHeaderViewOfSection;


typedef enum tableTag {

    tableOther  = 20, // 非主显示table
    tableMain   = 80,
    tableDetail = 81,
    tableFinal  = 1000
}  UITaleTag;

typedef enum moveOrenten{
    moveToLeft  =1,
    moveToRight
}moveOrenten;


static NSString * cellOtherID      =@"cellOtherID";
static NSString * cellMainDetailID =@"cellMainDetailID";
static NSString * cellFinalID      =@"cellFinalID";


NSString * mainTableSelctCellString =nil;
NSString * detailTableSelctCellSting =nil;


NSNumber * _detailCellID;                    //   选择的分类id
NSString * _LastdateTime;                  //    传入的时间
NSInteger _footerRefreshCount =0;         //     上啦加载此数
BOOL      _isHeaderRefresh   =NO;        //       是否在下拉刷新

int       _MaxCellCount    =500;           //    数组最大容量



@interface ARFurnitureViewController ()<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>


@property(nonatomic,strong)NSMutableArray      * tableDataArr;
@property(nonatomic,strong)NSMutableDictionary * tableDataDic;


@property(nonatomic,strong)NSMutableArray        * detailTableDataArr;
@property(nonatomic,strong)NSMutableArray      * furnitureListDataArrOfFinal;

@property(nonatomic,strong)NSMutableArray *tableViewVisiableArr;


@end

@implementation ARFurnitureViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // setting
    self.view.backgroundColor =[UIColor colorWithRed:139/255.0 green:139/255.0 blue:139/255.0 alpha:1];
    [self.closeBtn setButtonBorder:0.5 Radio:5 lineColor:nil];
    
    // 默认 main
    self.furnitureTable.tag =tableMain;
    UIView * temView =[[UIView alloc] init];
    temView.backgroundColor =[UIColor clearColor];
    self.furnitureTable.tableFooterView =temView;

    
    // 2.集成刷新控件
    [self setupRefresh];
    self.furnitureTable.footerHidden =YES;
    
    // 3 刚开始进入 强制刷新
    [self.furnitureTable headerBeginRefreshing];
    
    
    
}


#pragma mark - 下拉刷新 上拉加载
/**
 *  集成刷新控件
 */
- (void)setupRefresh
{
    // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.furnitureTable addHeaderWithTarget:self action:@selector(headerRereshing)];

//    self.furnitureTable.headerHidden =YES; // 可以禁止是否能刷新
    
    // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
    [self.furnitureTable addFooterWithTarget:self action:@selector(footerRereshing)];
}



#pragma mark 开始进入刷新状态
- (void)headerRereshing
{
    NSLog(@"%s",__func__);
    NSString *type =nil;
    if (self.furnitureTable.tag != tableFinal) {
        type = Kgetcategory;
    }else{
        type = Kgetproduct;
    }
    
    _isHeaderRefresh    =YES;
    _footerRefreshCount =0;
    
    
    NSMutableDictionary * dic =nil;
    if ([type isEqualToString:Kgetproduct]) {
        dic =[[NSMutableDictionary alloc] init];
        [dic setObject:[NSNumber numberWithInt:1] forKey:Kpage];
        [dic setObject:_detailCellID forKey:Kcategorycid]; // for test
    }
    
    [ARNetManage getJsonDataSuceess:^(BOOL success, id response){
        if (success) {
            if (self.furnitureTable.tag ==tableFinal) {
                self.furnitureListDataArrOfFinal    =response;
            }else{
                if ([response  isKindOfClass:[NSArray class]]) {
                    self.tableDataArr = response;
                }else if( [response isKindOfClass:[NSDictionary class]]){
                    self.tableDataDic = response;
                }
            }
             // 0.5秒后刷新表格UI
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // 刷新表格
                if (!self.furnitureTable.delegate) {
                    return ;
                }
                [self.furnitureTable reloadData];
                
                // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
                [self.furnitureTable headerEndRefreshing];
            });
            
            
            _isHeaderRefresh =NO;
        }
        
    } failture:^(BOOL failture){
        
        if (failture) {
            // do nothing
            self.furnitureListDataArrOfFinal =nil;
            self.tableDataArr               =nil;
            
            // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
            [self.furnitureTable headerEndRefreshing];
        }
        _isHeaderRefresh =NO;
        
    } withType:type  andUseForFuture:dic];
    
   
}

- (void)footerRereshing
{
    
    
    // 当且 tableview 是 final的时候，才能下拉加载
    if (self.furnitureTable.tag == tableFinal) {
        
        [self.furnitureTable headerEndRefreshing];
        _footerRefreshCount ++;
        
        [[ARNetManage getJsonDataSuceess]
        
    }

    
//    // 2.2秒后刷新表格UI
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        // 刷新表格
//        [self.furnitureTable reloadData];
//        
//        // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
//        [self.furnitureTable footerEndRefreshing];
//    });
}



#pragma mark - netWorking



#pragma mark - tableViewDelegate
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableFinal == tableView.tag) {
        CustomTableHeaderViewOfSection * sectionView =[[CustomTableHeaderViewOfSection alloc] init];
        [(UIButton *)sectionView.backBtn addTarget:self action:@selector(backToDetail:) forControlEvents:UIControlEventTouchUpInside];
        
        [(UILabel *)sectionView.titilLabel setText:detailTableSelctCellSting];
        
        return sectionView;
    }
    
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableFinal == tableView.tag) {
        return 44;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableFinal ==tableView.tag) {
        return 104;
    }
    
    return 44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag ==tableMain) {
         return self.tableDataArr.count;
    }else if(tableView.tag == tableDetail){
        return self.detailTableDataArr.count+1;// +1 是父节点 标题
    }else if (tableView.tag == tableFinal){
    
        return self.furnitureListDataArrOfFinal.count;
    }
    return 0;
   
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    UITableViewCell     *cell;
    
    if (tableView.tag ==tableMain) {
        cell =[tableView dequeueReusableCellWithIdentifier:cellMainDetailID];
        if (!cell) {
            cell =[[ARFurnitureCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellMainDetailID];
        }
//        cell.textLabel.text =[self.tableDataArr[indexPath.row] objectForKey:@"Value"];
//        cell.accessoryType =UITableViewCellAccessoryDetailDisclosureButton;
        
        [(ARFurnitureCell *)cell displayMainTable:self.tableDataArr[indexPath.row]];

        
    }else if(tableView.tag == tableDetail){
        cell =[tableView dequeueReusableCellWithIdentifier:cellMainDetailID];
        if (!cell) {
           cell =[[ARFurnitureCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellMainDetailID];
        }
        if (indexPath.row ==0) {
            [(ARFurnitureCell *)cell displayDetailTable:nil andParentTitle:mainTableSelctCellString andParentImageViewHidden:NO];
        }else{
            [(ARFurnitureCell *)cell displayDetailTable:self.detailTableDataArr[indexPath.row-1] andParentTitle:nil andParentImageViewHidden:YES];
        }
        
        
    }else if (tableView.tag ==tableFinal){
    
        cell = [tableView dequeueReusableCellWithIdentifier:cellFinalID];
        if (!cell) {
            cell =[[ARFunitureListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellFinalID];
        }
        [(ARFunitureListCell *)cell  disPlayViewUseDic:self.furnitureListDataArrOfFinal[indexPath.row] andotherP:nil];
    }
    
  
    cell.selectionStyle  =UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (!self.tableViewVisiableArr) {
        self.tableViewVisiableArr =[[NSMutableArray alloc] init];
    }
    UITableView *detailTable =nil;
    if (self.tableViewVisiableArr.count) {
        detailTable =self.tableViewVisiableArr[0];
    }else{
        detailTable = [[UITableView alloc] init];
        UIView * temView =[[UIView alloc] init];
        temView.backgroundColor =[UIColor clearColor];
        detailTable.tableFooterView =temView;
        
    }
    
//    [self.furnitureTable headerEndRefreshing];
//    [self.furnitureTable  footerEndRefreshing];
    [self cancelRequestWithTable:self.furnitureTable];
    self.furnitureTable =detailTable;
    [self setupRefresh];
    
    
    void (^settingDetailTabl)(BOOL headerHidden, BOOL footerhidden, id delegate, id datasource);
    settingDetailTabl =^(BOOL headerHidden, BOOL footerhidden, id delegate, id datasource){
        detailTable.delegate =delegate;
        detailTable.dataSource  =datasource;
        detailTable.headerHidden =headerHidden;
        detailTable.footerHidden =footerhidden;
    };
    
    
    
    void (^aniteFinish)(void);
    aniteFinish =^{
        [self.tableViewVisiableArr removeAllObjects];
       
        if ([tableView isKindOfClass:[UITableView class]]) {
            [self.tableViewVisiableArr addObject:tableView];
            tableView.delegate =nil;
            tableView.dataSource =nil;
            [tableView removeFromSuperview];
        }else{
            [self.tableViewVisiableArr addObject:(UITableView *)[self.view viewWithTag:tableFinal]];
            ((UITableView *)[self.view viewWithTag:tableFinal]).delegate =nil;
             ((UITableView *)[self.view viewWithTag:tableFinal]).dataSource =nil;
            [ ((UITableView *)[self.view viewWithTag:tableFinal]) removeFromSuperview];
        }
       
        
        detailTable.delegate    =self;
        detailTable.dataSource  =self;
        [detailTable reloadData];
    };
    
    void (^ detailTableOriFrame)(moveOrenten oren);
    detailTableOriFrame =^(moveOrenten oren){
        
        if ([tableView isKindOfClass:[UITableView class]]) {
            
        }else{
            
            UITableView *tableView = (UITableView *)[self.view viewWithTag:tableFinal];
            CGRect detailFrame;
            if (oren == moveToLeft) {
                detailFrame.origin.x =tableView.frame.origin.x + tableView.bounds.size.width;
            }else{
                detailFrame.origin.x =tableView.frame.origin.x - tableView.bounds.size.width;
            }
            
            detailFrame.origin.y = tableView.frame.origin.y;
            detailFrame.size.width  =tableView.bounds.size.width;
            detailFrame.size.height =tableView.bounds.size.height;
            detailTable.frame =detailFrame;
            return;
            
        }
        CGRect detailFrame;
        if (oren == moveToLeft) {
            detailFrame.origin.x =tableView.frame.origin.x + tableView.bounds.size.width;
        }else{
            detailFrame.origin.x =tableView.frame.origin.x - tableView.bounds.size.width;
        }
        
        detailFrame.origin.y = tableView.frame.origin.y;
        detailFrame.size.width  =tableView.bounds.size.width;
        detailFrame.size.height =tableView.bounds.size.height;
        detailTable.frame =detailFrame;
    };

    
    if ([tableView isKindOfClass:[UITableView class]]) {
        if (tableView.tag == tableMain) {
            
            mainTableSelctCellString = [(ARFurnitureCell *)[tableView  cellForRowAtIndexPath:indexPath] categoryNameLabel].text;
            
            NSArray *childArr =[self.tableDataArr[indexPath.row] objectForKey:KChildCategory];
            
            
            // 进入详细 判断能否进入详情
            if (childArr&&[childArr isKindOfClass:[NSArray class]]&&childArr.count>0) {
                
                self.detailTableDataArr = (NSMutableArray *)childArr;
                
                settingDetailTabl(YES,YES,nil,nil);
                detailTable.tag =tableDetail;
                
                detailTableOriFrame(moveToLeft);
                
//                tableView.delegate =nil;
//                tableView.dataSource =nil;
                
                [tableView.superview addSubview:detailTable];
                
                [UIView animateWithDuration:KDuringTime animations:^{
                    CGRect hiddenframe =[self finalFrameWithTable:tableView hidden:YES orenten:moveToLeft];
                    tableView.frame =hiddenframe;
                    
                    CGRect showFram = [self finalFrameWithTable:detailTable hidden:NO orenten:moveToLeft];
                    detailTable.frame =showFram;
                    
                    
                } completion:^(BOOL finish){
                    if (finish) {
                        aniteFinish();
                    }
                }];
            }else{
                // 直接进入final
                
                settingDetailTabl(YES,YES,nil,nil);
                detailTable.tag =tableFinal;
                detailTableOriFrame(moveToLeft);
//                tableView.delegate =nil;
//                tableView.dataSource =nil;
                [tableView.superview addSubview:detailTable];
                
                [UIView animateWithDuration:KDuringTime animations:^{
                    CGRect hiddenframe =[self finalFrameWithTable:tableView hidden:YES orenten:moveToLeft];
                    tableView.frame =hiddenframe;
                    
                    CGRect showFram = [self finalFrameWithTable:detailTable hidden:NO orenten:moveToLeft];
                    detailTable.frame =showFram;
                    
                } completion:^(BOOL finish){
                    if (finish) {
                        aniteFinish();
                    }
                }];
                
            }
        }
        
        
        else if (tableView.tag == tableDetail){
            
            if (indexPath.row ==0) {
                // 点击第一行，返回上一层
                settingDetailTabl(NO,YES,nil,nil);
                detailTable.tag =tableMain;
                detailTableOriFrame(moveToRight);
//                tableView.delegate =nil;
//                tableView.dataSource =nil;
                [tableView.superview addSubview:detailTable];
                
                [UIView animateWithDuration:KDuringTime animations:^{
                    CGRect hiddenframe =[self finalFrameWithTable:tableView hidden:YES orenten:moveToRight];
                    tableView.frame =hiddenframe;
                    
                    CGRect showFram = [self finalFrameWithTable:detailTable hidden:NO orenten:moveToRight];
                    detailTable.frame =showFram;
                    
                } completion:^(BOOL finish){
                    if (finish) {
                        aniteFinish();
                    }
                }];
            }else{
                // 进入list
                detailTableSelctCellSting = [(ARFurnitureCell *)[tableView  cellForRowAtIndexPath:indexPath] categoryNameLabel].text;
                _detailCellID   =[self.detailTableDataArr[indexPath.row -1] objectForKey:@"ID"];
                // 进入 list ,fanail ,list表 需要 searchbar
                settingDetailTabl(NO,NO,nil,nil);
                detailTable.tag =tableFinal;
                detailTableOriFrame(moveToLeft);
//                tableView.delegate =nil;
//                tableView.dataSource =nil;
                [tableView.superview addSubview:detailTable];
                [UIView animateWithDuration:KDuringTime delay:0 options:UIViewAnimationOptionCurveEaseInOut  animations:^{
                    CGRect hiddenframe =[self finalFrameWithTable:tableView hidden:YES orenten:moveToLeft];
                    tableView.frame =hiddenframe;
                    //list表 需要 searchbar
                    CGRect showFram = [self finalFrameWithTable:detailTable hidden:NO orenten:moveToLeft];
                    showFram.origin.y =self.searchBar.frame.origin.y;
                    showFram.size.height +=self.searchBar.frame.size.height;
                    detailTable.frame =showFram;
                }  completion:^(BOOL FINISH){
                    if (FINISH) {
                        aniteFinish();
                        [detailTable headerBeginRefreshing];
                    }
                }];
                
            }
            
            
        }
        
        else if (tableView.tag == tableFinal){
            ARListCellVC * listVC =[[ARListCellVC alloc] init];
           
            
            [self presentViewController:listVC animated:YES completion:nil];
            
            
        }

    }else if ([tableView isKindOfClass:[UIButton class]]){
        // 需要返回detail
        //返回上一层
        settingDetailTabl(YES ,YES,nil,nil);
        detailTable.tag =tableDetail;
        detailTableOriFrame(moveToRight);
        ((UITableView *)[self.view viewWithTag:tableFinal]).delegate =nil;
        ((UITableView *)[self.view viewWithTag:tableFinal]).dataSource =nil;
        [((UITableView *)[self.view viewWithTag:tableFinal]).superview addSubview:detailTable];
        
        [UIView animateWithDuration:KDuringTime animations:^{
            CGRect hiddenframe =[self finalFrameWithTable:((UITableView *)[self.view viewWithTag:tableFinal]) hidden:YES orenten:moveToRight];
            tableView.frame =hiddenframe;
            
            CGRect showFram = [self finalFrameWithTable:detailTable hidden:NO orenten:moveToRight];
            showFram.origin.y = self.searchBar.frame.origin.y+ self.searchBar.bounds.size.height;
            showFram.size.height    -=self.searchBar.bounds.size.height;
            detailTable.frame =showFram;
            
        } completion:^(BOOL finish){
            if (finish) {
                aniteFinish();
            }
        }];

        
    
    }
    
    
    
}


#pragma mark - scrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{

}

-(void)lazyLoadScroll:(UITableView *)table{

}







#pragma mark - 计算各自新的frame
-(CGRect)finalFrameWithTable:(UITableView *)table hidden:(BOOL)hidden orenten:(moveOrenten)oren{
    CGRect frame;
    CGSize size =table.bounds.size;
    if (hidden) {
        if (oren ==moveToLeft) {
             frame.origin.x = -size.width;
        }else if (moveToLeft == moveToRight){
            frame.origin.x  = size.width;
        }
       
    }else{
        frame.origin.x =0;
    }
    
    frame.origin.y =table.frame.origin.y;
    frame.size =size;
    return frame;
    
}

#pragma mark - cancelreques
-(void)cancelRequestWithTable:(UITableView *)table{
    [ARNetManage cancelAllRequest];
//    table.delegate =nil;
//    table.dataSource =nil;
    [table headerEndRefreshing];
    [table footerEndRefreshing];

}


#pragma mark - searchBar
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"%s",__func__);
}



#pragma mark - buttonClick

- (IBAction)buttonClickTap:(UIButton *)sender {

    if (self.closeBtn == sender) {
        if (self.parentViewController&&[self.parentViewController isKindOfClass:NSClassFromString(@"ARRootViewController")]) {
            [(ARRootViewController *)self.parentViewController showLeftCoreShowView:NO];
        }
        
    }
}

-(void)backToDetail:(UIButton *)btn{
    if ([btn isKindOfClass:[UIButton class]]) {
        [self tableView:btn didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:-500 inSection:-500]]; //此数随意
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end





@implementation CustomTableHeaderViewOfSection

-(id)init{
    if (self =[super init]) {
        UIButton *btn =[UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor =[UIColor clearColor];
        [btn setImage:[UIImage imageNamed:@"caidanfanhui"] forState:UIControlStateNormal];
        //[btn addTarget:self action:@selector(backToDetail:) forControlEvents:UIControlEventTouchUpInside];
        
        _btn =btn;
        btn.frame =(CGRect){11,12,[UIImage imageNamed:@"caidanfanhui"].size.width,[UIImage imageNamed:@"caidanfanhui"].size.height};
        [self addSubview:btn];
        
        
        UILabel *titil =[[UILabel alloc] init];
        titil.backgroundColor =[UIColor clearColor];
        titil.font =[UIFont boldSystemFontOfSize:16];
        _title =titil;
        
        return self;
    }
    return nil;
}

-(id)backBtn{
    return _btn;
}
-(id)titilLabel{
    return _title;
}
@end


