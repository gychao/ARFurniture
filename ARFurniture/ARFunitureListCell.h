//
//  ARFunitureListCell.h
//  ARFurniture
//
//  Created by dilitech on 14-7-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARFunitureListCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UILabel *titileLabel;


@property (weak, nonatomic) IBOutlet UILabel *priceLabel;


@property (weak, nonatomic) IBOutlet UIImageView *detailView;


@property (weak, nonatomic) IBOutlet UIImageView *picImageView;



-(void)disPlayViewUseDic:(NSDictionary *)dic  andotherP:(id)usefuture;


@end
