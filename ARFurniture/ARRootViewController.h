//
//  ARRootViewController.h
//  ARFurniture
//
//  Created by dilitech on 14-7-15.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"

typedef enum selectStatus{
    selectHidden        =1,
    select_NoHidden     =1<1,
    noSelect_NoHidden   =1<<2
}UISelectStatus;


@interface ARRootViewController : SCBaseViewController<UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet UIButton *ARButton;

@property (weak, nonatomic) IBOutlet UIButton *SaveButton;


@property (weak, nonatomic) IBOutlet UIView *CoreShowBgView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


#pragma mark -家具方案
@property (weak, nonatomic) IBOutlet UIView *controlBgView;
@property (weak, nonatomic) IBOutlet UIButton *furnitureButton;
@property (weak, nonatomic) IBOutlet UIButton *programButton;


#pragma mark - 回调
-(void)showLeftCoreShowView:(BOOL)isShow;

@end
