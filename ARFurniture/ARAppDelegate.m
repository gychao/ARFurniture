//
//  ARAppDelegate.m
//  ARFurniture
//
//  Created by dilitech on 14-7-15.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "ARAppDelegate.h"

#import "ARLaunchManger.h"

@interface ARAppDelegate ()


@property(nonatomic,strong)UIView *leftControlView;
@property(nonatomic,strong)UIButton *ARButton, *saveButton;

@end

@implementation ARAppDelegate
/*!
 *
 *
 *  @param otherPara  can be nil , use for future
 */
-(void)startApp:(id)otherPara{
    
    
    
    // 刚启动程序 加载gif 动画 // 等数据加载完毕的时候
    [ARLaunchManger  startApp:self];
}


-(void)click{
    NSLog(@"%s",__func__);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // need delete
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    

    [self startApp:self];
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
