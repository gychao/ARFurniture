//
//  UITabBarController+autoRotate.h
//  Furniture_basic
//
//  Created by ziheli on 12-11-26.
//
//

#import <UIKit/UIKit.h>

@interface UITabBarController (autoRotate)
#ifdef __IPHONE_6_0
-(BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
#endif
@end
