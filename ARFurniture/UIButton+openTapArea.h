//
//  UIButton+openTapArea.h
//  SleepCareII
//
//  Created by dilitech on 14-7-3.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//


// button 扩大点击区域
#import <UIKit/UIKit.h>

@interface UIButton (openTapArea)

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent*)event;

-(void)setButtonBorder:(float)lineWidth Radio:(float)radio lineColor:(UIColor *)clor;

@end
