//
//  ARNetManage.m
//  ARFurniture
//
//  Created by dilitech on 14-7-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "ARNetManage.h"


#import "AFHTTPRequestOperationManager.h"
#import "JSONKit.h"





@implementation ARNetManage


+(ARNetManage *)shareNetManage{
    static dispatch_once_t once;
    static ARNetManage * shareManage;
    dispatch_once(&once,^{ shareManage =[[self alloc] init];});
    return shareManage;
}

+(void)getJsonDataSuceess:(void (^)(BOOL, id))suceess failture:(void (^)(BOOL))faileture withType:(NSString *)type andUseForFuture:(id)future{
    ARNetManage *manager =[self shareNetManage];
    
    manager.suceess   =nil;
    manager.failtrue  =nil;
    
    manager.suceess   =suceess;
    manager.failtrue  =faileture;
    
//    if (1) {
//        // for no net working test
//        NSString *str =[[NSBundle mainBundle] pathForResource:@"jiajufenlei" ofType:@"txt"];
//        NSString *js =[[NSString alloc] initWithContentsOfFile:str encoding:NSUTF8StringEncoding error:nil];
////        NSData * dat =[NSData dataWithContentsOfFile:str];
//        NSData *data =[js JSONData];
//        
//        
////         NSArray *ar = [[NSArray alloc] ];
////       manager.suceess(YES,ar);
//        return ;
//    }
    
    if (manager.requestOprationArr ==nil) {
        manager.requestOprationArr =[[NSMutableArray alloc] init];
    }
    [self cancelAllRequest]; // 先取消 所有
    
    AFHTTPRequestOperationManager *RequstManager = [AFHTTPRequestOperationManager manager];
    RequstManager.requestSerializer = [AFJSONRequestSerializer serializer];
    RequstManager.responseSerializer = [AFJSONResponseSerializer serializer];
    [RequstManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [RequstManager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [manager.requestOprationArr addObject:RequstManager];
    NSString *urlstr =[SCShareFunc getServerUrlString];
    
    NSDictionary *para =nil;
    if ([type isEqualToString:Kgetcategory]) {
        para =@{Ktype:Kgetcategory};
    }else if ([type isEqualToString:Kgetproduct]){
       
        [(NSMutableDictionary *)future setObject:Kgetproduct forKey:Ktype];
        para =future;
    }
    
   
    
    [RequstManager GET:urlstr parameters:para success:^(AFHTTPRequestOperation *op, id respose){
        NSLog(@"%@",respose);
        if ([NSJSONSerialization isValidJSONObject:respose]) {
//            NSError *err;
          
         
          
            NSArray * responseArr = respose;//[NSJSONSerialization JSONObjectWithData:respose options:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&err];//[[JSONDecoder decoder] objectWithData:respose];
            // test
            if ([type isEqualToString:Kgetcategory]) {
                
                NSString *path =@"/Users/dilitech/Desktop/jiajufenlei.txt";
                [responseArr writeToFile:path atomically:YES]; // 数组就是一个xml
                
            }else if ([type isEqualToString:Kgetproduct]){
                
                NSLog(@"response : %@", respose);
                if ([[respose objectForKey:KResult] intValue] ==1) {
                    responseArr =[respose objectForKey:KProductList];
                }else{
                    responseArr =nil;
                }
                NSString *path =@"/Users/dilitech/Desktop/jiajuliebiao.txt";
                [responseArr writeToFile:path atomically:YES]; // 数组就是一个xml
                
            }
          
            
            
            if (responseArr&&responseArr.count>0) {
                if (manager.suceess) {
                    manager.suceess(YES,responseArr);
                }
            }else{
                
                if (manager.failtrue) {
                    manager.failtrue(YES);
                }
            }
        }
        
       
        
        
        [manager.requestOprationArr removeObject:RequstManager];
        
    } failure:^(AFHTTPRequestOperation * op, NSError *err){
        
     
        NSLog(@"err: %@",err.userInfo);
        if (manager.failtrue) {
            manager.failtrue(YES);
        }
        
        [manager.requestOprationArr removeObject:RequstManager];
    }];


}

+(void)cancelAllRequest{
    ARNetManage *manager =[self shareNetManage];
    if (manager.requestOprationArr ==nil) {
        return;
    }
    for (AFHTTPRequestOperationManager * mange in manager.requestOprationArr) {
        [mange.operationQueue cancelAllOperations];
    }
    
    [manager.requestOprationArr removeAllObjects];
}

@end
