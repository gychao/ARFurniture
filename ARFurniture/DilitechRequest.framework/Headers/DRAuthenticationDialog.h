//
//  DRAuthenticationDialog.h
//  Part of DilitechRequest -> http://allseeing-i.com/DilitechRequest
//
//  Created by Ben Copsey on 21/08/2009.
//  Copyright 2009 All-Seeing Interactive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class DilitechRequest;

typedef enum _DRAuthenticationType {
	DRStandardAuthenticationType = 0,
    DRProxyAuthenticationType = 1
} DRAuthenticationType;

@interface DRAutorotatingViewController : UIViewController
@end

@interface DRAuthenticationDialog : DRAutorotatingViewController <UIActionSheetDelegate, UITableViewDelegate, UITableViewDataSource> {
	DilitechRequest *request;
	DRAuthenticationType type;
	UITableView *tableView;
	UIViewController *presentingController;
	BOOL didEnableRotationNotifications;
}
+ (void)presentAuthenticationDialogForRequest:(DilitechRequest *)request;
+ (void)dismiss;

@property (retain) DilitechRequest *request;
@property (assign) DRAuthenticationType type;
@property (assign) BOOL didEnableRotationNotifications;
@property (retain, nonatomic) UIViewController *presentingController;
@end
