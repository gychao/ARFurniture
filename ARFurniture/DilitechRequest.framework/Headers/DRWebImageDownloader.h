/*
 * This file is part of the DRWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import <Foundation/Foundation.h>
#import "DRWebImageDownloaderDelegate.h"
#import "DRWebImageCompat.h"

extern NSString *const DRWebImageDownloadStartNotification;
extern NSString *const DRWebImageDownloadStopNotification;

@interface DRWebImageDownloader : NSObject
{
    @private
    NSURL *url;
    id<DRWebImageDownloaderDelegate> delegate;
    NSURLConnection *connection;
    NSMutableData *imageData;
	id userInfo;
}

@property (nonatomic, retain) NSURL *url;
@property (nonatomic, assign) id<DRWebImageDownloaderDelegate> delegate;
@property (nonatomic, retain) NSMutableData *imageData;
@property (nonatomic, retain) id userInfo;

+ (id)downloaderWithURL:(NSURL *)url delegate:(id<DRWebImageDownloaderDelegate>)delegate userInfo:(id)userInfo;
+ (id)downloaderWithURL:(NSURL *)url delegate:(id<DRWebImageDownloaderDelegate>)delegate;
- (void)start;
- (void)cancel;

// This method is now no-op and is deprecated
+ (void)setMaxConcurrentDownloads:(NSUInteger)max __attribute__((deprecated));

@end
