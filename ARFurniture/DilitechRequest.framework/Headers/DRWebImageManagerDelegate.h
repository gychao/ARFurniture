/*
 * This file is part of the DRWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

@class DRWebImageManager;

@protocol DRWebImageManagerDelegate <NSObject>

@optional

- (void)webImageManager:(DRWebImageManager *)imageManager didFinishWithImage:(UIImage *)image;

@end
