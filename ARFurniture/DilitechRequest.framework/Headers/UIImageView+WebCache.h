/*
 * This file is part of the DRWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "DRWebImageCompat.h"
#import "DRWebImageManagerDelegate.h"


@interface UIImageView (WebCache) <DRWebImageManagerDelegate>
{
}

-(void)setImageWithURL:(NSURL *)url;
- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder;
- (void)cancelCurrentImageLoad;

@end
