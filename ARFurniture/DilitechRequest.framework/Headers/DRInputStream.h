//
//  DRInputStream.h
//  Part of DilitechRequest -> http://allseeing-i.com/DilitechRequest
//
//  Created by Ben Copsey on 10/08/2009.
//  Copyright 2009 All-Seeing Interactive. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DilitechRequest;

// This is a wrapper for NSInputStream that pretends to be an NSInputStream itself
// Subclassing NSInputStream seems to be tricky, and may involve overriding undocumented methods, so we'll cheat instead.
// It is used by DilitechRequest whenever we have a request body, and handles measuring and throttling the bandwidth used for uploading

@interface DRInputStream : NSObject {
	NSInputStream *stream;
	DilitechRequest *request;
}
+ (id)inputStreamWithFileAtPath:(NSString *)path request:(DilitechRequest *)request;
+ (id)inputStreamWithData:(NSData *)data request:(DilitechRequest *)request;

@property (retain, nonatomic) NSInputStream *stream;
@property (assign, nonatomic) DilitechRequest *request;
@end
