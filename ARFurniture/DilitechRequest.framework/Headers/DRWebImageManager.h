/*
 * This file is part of the DRWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "DRWebImageCompat.h"
#import "DRWebImageDownloaderDelegate.h"
#import "DRWebImageManagerDelegate.h"
#import "DRImageCacheDelegate.h"

@interface DRWebImageManager : NSObject <DRWebImageDownloaderDelegate, DRImageCacheDelegate>
{
    NSMutableArray *delegates;
    NSMutableArray *downloaders;
    NSMutableDictionary *downloaderForURL;
    NSMutableArray *failedURLs;
}

+ (id)sharedManager;
- (UIImage *)imageWithURL:(NSURL *)url;
- (void)downloadWithURL:(NSURL *)url delegate:(id<DRWebImageManagerDelegate>)delegate;
- (void)downloadWithURL:(NSURL *)url delegate:(id<DRWebImageManagerDelegate>)delegate retryFailed:(BOOL)retryFailed;
- (void)cancelForDelegate:(id<DRWebImageManagerDelegate>)delegate;

@end
