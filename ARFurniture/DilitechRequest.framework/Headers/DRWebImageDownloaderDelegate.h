/*
 * This file is part of the DRWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "DRWebImageCompat.h"

@class DRWebImageDownloader;

@protocol DRWebImageDownloaderDelegate <NSObject>

@optional

- (void)imageDownloaderDidFinish:(DRWebImageDownloader *)downloader;
- (void)imageDownloader:(DRWebImageDownloader *)downloader didFinishWithImage:(UIImage *)image;
- (void)imageDownloader:(DRWebImageDownloader *)downloader didFailWithError:(NSError *)error;

@end
