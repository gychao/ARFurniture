//
//  SCBaseViewController.h
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCBaseViewController : UIViewController<UIScrollViewDelegate>

#ifdef __IPHONE_6_0
-(BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
#endif

-(void)dealloc;
@end
