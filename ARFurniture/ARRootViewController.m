//
//  ARRootViewController.m
//  ARFurniture
//
//  Created by dilitech on 14-7-15.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "ARRootViewController.h"

#import  "ARFurnitureViewController.h"



#define   KScrollSubViewNum     2

@interface ARRootViewController ()


@property(nonatomic,strong)ARFurnitureViewController *furnitureVC;


@end

@implementation ARRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

    
    // 默认 是 hidden
    [self setFurBtnColorUseStatus:selectHidden andParBtnColor:selectHidden];
    
    [self setButtonBorder:1 Radio:5 lineColor:[UIColor whiteColor] forBtn:self.ARButton];
    [self setButtonBorder:1 Radio:5 lineColor:[UIColor whiteColor] forBtn:self.SaveButton];
    [self setArbtnHidden:NO SaveBtn:YES];
    
    
    //
    self.furnitureVC  = [[ARFurnitureViewController alloc] init];
    
    self.furnitureVC.view.frame =(CGRect){0,0,self.scrollView.bounds.size.width,self.scrollView.bounds.size.height};
    [self addChildViewController:self.furnitureVC];
    
    self.scrollView.contentSize =(CGSize){self.scrollView.bounds.size.width*KScrollSubViewNum,self.scrollView.bounds.size.height};
    
    
    [self.scrollView addSubview:self.furnitureVC.view];
    
    CGRect frame = self.CoreShowBgView.frame;
    frame.origin.x -=frame.size.width;
    self.CoreShowBgView.frame =frame;
    
    
    
}


#pragma mark - buttonAction
-(IBAction)buttonClick:(UIButton *)btn{
    if (btn == self.furnitureButton|| btn ==self.programButton) {
        // 方案与家具
        if (self.CoreShowBgView.frame.origin.x>=0) {
            // 已经在显示
            if (btn.selected) {
                return;
            }
            btn.selected =YES;
            if (btn ==self.furnitureButton) {
                [self.scrollView setContentOffset:(CGPoint){0,0} animated:YES];
                self.programButton.selected =NO;
                [self setFurBtnColorUseStatus:select_NoHidden andParBtnColor:noSelect_NoHidden];
            }else if(btn ==self.programButton){
                [self.scrollView setContentOffset:(CGPoint){self.scrollView.bounds.size.width,0} animated:YES];
                self.furnitureButton.selected =NO;
                [self setFurBtnColorUseStatus:noSelect_NoHidden andParBtnColor:select_NoHidden];
            }
            
        }else{
            // 没有 显示,需要显示
            [UIView animateWithDuration:KDuringTime animations:^{
                
                self.CoreShowBgView.frame =(CGRect){0,0,self.CoreShowBgView.bounds.size.width,self.CoreShowBgView.bounds.size.height};
                
            } completion:^(BOOL finish){
                if (finish) {
                    if (btn ==self.furnitureButton) {
                        [self.scrollView setContentOffset:(CGPoint){0,0} animated:YES];
                        btn.selected =YES;
                        self.programButton.selected =NO;
                        [self setFurBtnColorUseStatus:select_NoHidden andParBtnColor:noSelect_NoHidden];
                    }else if(btn ==self.programButton){
                        [self.scrollView setContentOffset:(CGPoint){self.scrollView.bounds.size.width,0} animated:YES];
                        btn.selected =YES;
                        self.furnitureButton.selected =NO;
                        [self setFurBtnColorUseStatus:noSelect_NoHidden andParBtnColor:select_NoHidden];
                    }
                    
                   
                    self.CoreShowBgView.layer.shadowColor =[UIColor blackColor].CGColor;
                    self.CoreShowBgView.layer.shadowOffset =(CGSize){0,0};
                    self.CoreShowBgView.layer.shadowOpacity = 0.5;
                    
                    self.controlBgView.layer.shadowColor =[UIColor blackColor].CGColor;
                    self.controlBgView.layer.shadowOffset =(CGSize){0,1};
                    self.controlBgView.layer.shadowOpacity = 1;

             
                }
            }];
        }
    }
    
    
}


-(void)showLeftCoreShowView:(BOOL)isShow{
    float x;
    if (isShow) {
        x =0;
    }else{
        x = -self.CoreShowBgView.bounds.size.width;
    }
    // 没有 显示,需要显示
    [UIView animateWithDuration:KDuringTime animations:^{
        
        self.CoreShowBgView.frame =(CGRect){x,0,self.CoreShowBgView.bounds.size.width,self.CoreShowBgView.bounds.size.height};
        
    } completion:^(BOOL finish){
        if (finish) {
            if (!isShow) {
                [self setFurBtnColorUseStatus:selectHidden andParBtnColor:selectHidden];
            }
        }
    }];

}
#pragma mark - buttonSetting
-(void)setFurBtnColorUseStatus:(UISelectStatus)status andParBtnColor:(UISelectStatus)statusP{
   
    if (status ==selectHidden) {
        self.furnitureButton.backgroundColor =KColorHidden;
        [self.furnitureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.furnitureButton.selected =NO;
    }else if (status ==select_NoHidden){
        self.furnitureButton.backgroundColor =KColorNoHidden_Select;
       [self.furnitureButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        self.furnitureButton.selected =YES;

    }else if (status ==noSelect_NoHidden){
        self.furnitureButton.backgroundColor =KColorNoHidden_NOSelect;
         [self.furnitureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.furnitureButton.selected =NO;

    }
    
    
    if (statusP ==selectHidden) {
        self.programButton.backgroundColor =KColorHidden;
        [self.programButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.programButton.selected =NO;

    }else if (statusP ==select_NoHidden){
        self.programButton.backgroundColor =KColorNoHidden_Select;
         [self.programButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        self.programButton.selected =YES;
    }else if (statusP ==noSelect_NoHidden){
        self.programButton.backgroundColor =KColorNoHidden_NOSelect;
        [self.programButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.programButton.selected =NO;
    }

}
-(void)setArbtnHidden:(BOOL)hiddenA SaveBtn:(BOOL)hiddenS{
    self.ARButton.hidden    =hiddenA;
    self.SaveButton.hidden  =hiddenS;
}
-(void)setButtonBorder:(float)lineWidth Radio:(float)radio lineColor:(UIColor *)clor forBtn:(UIButton *)btn{
    btn.layer.borderWidth   =lineWidth;
    btn.layer.borderColor   =clor.CGColor;
    btn.layer.cornerRadius  =radio;
}

#pragma mark -ScrollDelegate
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    NSLog(@"%s",__func__);
    if (decelerate) {
        
    }else{
        [self scrollView:scrollView needLoadView:nil];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
 NSLog(@"%s",__func__);
    [self scrollView:scrollView needLoadView:nil];
}

-(void)scrollView:(UIScrollView *)scrollView  needLoadView:(id)future{
    // 获取当前scroll 便宜量
    int index = scrollView.contentOffset.x/scrollView.bounds.size.width;
    if (index==0) {
        [self setFurBtnColorUseStatus:select_NoHidden andParBtnColor:noSelect_NoHidden];
    }else if (index ==1){
        [self setFurBtnColorUseStatus:noSelect_NoHidden andParBtnColor:select_NoHidden];
    }
    
}



#pragma mark - touch
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"%s",__func__);
    id oter =self.nextResponder;
    while (oter) {
        NSLog(@"%@",NSStringFromClass([oter class]));
        oter =((UIResponder *)oter).nextResponder;
    }
    [self.nextResponder touchesBegan:touches withEvent:event];
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.nextResponder touchesMoved:touches withEvent:event];
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.nextResponder touchesEnded:touches withEvent:event];

}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.nextResponder touchesCancelled:touches withEvent:event];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
