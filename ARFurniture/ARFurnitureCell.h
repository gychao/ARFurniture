//
//  ARFurnitureCell.h
//  ARFurniture
//
//  Created by dilitech on 14-7-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARFurnitureCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *presentImageView;

@property (weak, nonatomic) IBOutlet UIImageView *detailView;


-(void)displayMainTable:(NSDictionary *)dic;

-(void)displayDetailTable:(NSDictionary *)dic andParentTitle:(NSString *)paraTitle andParentImageViewHidden:(BOOL)hidden;

@end
