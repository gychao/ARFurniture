//
//  ARLaunchManger.h
//  ARFurniture
//
//  Created by dilitech on 14-7-15.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ARAppDelegate;
@interface ARLaunchManger : NSObject


+(void) startApp:(ARAppDelegate *)appdelegate;

+(void) loadGif:(ARAppDelegate *)appdelegate;

@end
