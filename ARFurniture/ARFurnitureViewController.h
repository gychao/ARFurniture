//
//  ARFurnitureViewController.h
//  ARFurniture
//
//  Created by dilitech on 14-7-16.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"

@interface ARFurnitureViewController : SCBaseViewController



@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *furnitureTable;



@end


// for tableHeader tablefinal
@interface CustomTableHeaderViewOfSection : UIView{
    
@private
    __weak UILabel  * _title;
    __weak UIButton * _btn;
}

-(id)backBtn;
-(id)titilLabel;
-(id)init;
@end