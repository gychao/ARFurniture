//
//  ARFunitureListCell.m
//  ARFurniture
//
//  Created by dilitech on 14-7-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "ARFunitureListCell.h"

#import "AFNetworking/AFNetworking.h"
#import "UIImageView+AFNetworking.h"


#define KFunitureList @"FunitureList"


#define KTitle      @"Title"
#define KName       @"Name"
#define KImagePath  @"Image"
#define KPrice      @"Price"
#define KUpdate     @"Update"

@implementation ARFunitureListCell



-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self =[[[NSBundle mainBundle] loadNibNamed:@"ARFunitureListCell" owner:nil options:nil] lastObject];
    return self;

}


- (void)awakeFromNib
{
    // Initialization code
    NSLog(@"%s",__func__);
}

-(void)disPlayViewUseDic:(NSDictionary *)dic andotherP:(id)usefuture{
    self.titileLabel.text =[NSString stringWithFormat:@"%@ \n\n",[dic objectForKey:KName]];
    self.priceLabel.text  =[NSString stringWithFormat:@"¥ %.0f",[[dic objectForKey:KPrice] floatValue]];
    NSURL *url =[NSURL URLWithString:[dic objectForKey:KImagePath]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    if (url) {
        [self.picImageView      setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"118"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
            if (image.size.width>5) {
                self.picImageView.image =image;
            }
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
            
        }];
    }
}

//// 配置View  多弄个xib 吧
//-(void)settingItSViewUseTag:(NSString *)identy{
//    
//    [self.contentView.subviews makeObjectsPerformSelector:@selector(setHidden:) withObject:@YES];
//    
//    
//    if ([identy isEqualToString:KFunitureList]) {
//        CGRect frame = self.frame;
//        frame.size.height = 104;
//        
//        
//    }
//
//}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
