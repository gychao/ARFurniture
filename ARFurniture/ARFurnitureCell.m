//
//  ARFurnitureCell.m
//  ARFurniture
//
//  Created by dilitech on 14-7-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "ARFurnitureCell.h"


@implementation ARFurnitureCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self =[[[NSBundle mainBundle] loadNibNamed:@"FurnitureCell" owner:nil options:nil] lastObject];
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)displayMainTable:(NSDictionary *)dic{
    self.categoryNameLabel.text =[dic objectForKey:KValue];
    self.presentImageView.hidden    =YES;
    self.detailView.hidden          =NO;
    self.categoryNameLabel.textColor =[UIColor blackColor];
}

-(void)displayDetailTable:(NSDictionary *)dic andParentTitle:(NSString *)paraTitle andParentImageViewHidden:(BOOL)hidden{

    self.presentImageView.hidden =hidden;
    if (hidden ==NO) {
        self.categoryNameLabel.text =paraTitle;
        self.detailView.hidden  =YES;
        self.categoryNameLabel.textColor =[UIColor blackColor];
        
    }else{
        self.detailView.hidden =NO;
        self.categoryNameLabel.text =[dic objectForKey:KValue];
        self.categoryNameLabel.textColor    =[UIColor grayColor];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
