//
//  main.m
//  ARFurniture
//
//  Created by dilitech on 14-7-15.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ARAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ARAppDelegate class]));
    }
}
