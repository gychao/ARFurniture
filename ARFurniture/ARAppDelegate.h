//
//  ARAppDelegate.h
//  ARFurniture
//
//  Created by dilitech on 14-7-15.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARAppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;

@end
