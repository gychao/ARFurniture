//
//  ARNetManage.h
//  ARFurniture
//
//  Created by dilitech on 14-7-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <Foundation/Foundation.h>


#define Ktype         @"type"
#define Kgetcategory  @"getcategory"

#define Kgetproduct   @"getproductlist"
#define Kdate         @"date"
#define Kpage          @"page"
#define Kcategorycid    @"categorycid"

#define KResult       @"Result"
#define KProductList    @"ProductList"



@interface ARNetManage : NSObject

@property(nonatomic,strong)void (^suceess)(BOOL success,   id  reponse);
@property(nonatomic,strong)void (^failtrue)(BOOL failture);

@property(nonatomic,strong)NSMutableArray * requestOprationArr;

+(ARNetManage *)shareNetManage;


// 将date page categorycid 装在future 中,,type  是getproduct 的时候。。future 是字典 
+(void)getJsonDataSuceess:(void(^)(BOOL success,id response))suceess  failture:(void(^)(BOOL faileture))faileture withType:(NSString *)type andUseForFuture:(id)future;


+(void)cancelAllRequest;
@end
